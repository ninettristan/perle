# perle

## Contexte

Le projet PERLE a pour objectif de créer une plate-forme d'émulation de
l'interface radio LTE.

Plus précisément, il s'agit d'en reproduire les caractéristiques de latence, de
pertes, de mise en forme et d’ordonnancement.

Pour ce faire, l'idée est d'utiliser le mécanisme de qdisc, disponible
nativement dans le noyau Linux, afin de reproduire les bearers LTE.

Perle configure la machine Linux sur laquelle il est exécuté pour que, utilisée
en tant que bridge, elle traite les flux la traversant comme l'aurait fait LTE.

En fait, LTE met en place des canaux logiques de traitement de QoS, appelés
bearers, assez semblables aux qdiscs du noyau Linux : des files d'attente
paramétrables offrant du traitement de QoS.

Ainsi, le programme, réalisé en python, met en place une architecture qdisc qui
traduit une liste de bearers fournie par son utilisateur.

Plus précisément, en présence de flux concurrents, Perle introduit de la
latence, des pertes, et effectue de la mise en forme et de l’ordonnancement de
la même manière que l'aurait fait LTE.

## Pré-requis

Il faut au préalable configurer la machine comme un pont avec le paquet
bridge-utils.

## Utilisation

Perle prend en argument un fichier de configuration où l'utilisateur renseigne
la liste des bearers qu'il souhaite émuler et leurs caractéristiques, et met en
place une structure qdisc reproduisant assez fidèlement leur comportement.

Il doit être lancé en tant que super-utilisateur.

Les bearers sont accessibles aux flux entrants grâce à leur port de destination.
Le flux dont le port de destination est 5000+n ira dans le bearer n.

Usage:

	perle.py [options] CONF

where CONF is the csv configuration file.

Example of configuration file :

        "qci";"gbr";"mbr"
        5;;9000
        1;1000;4000
        3;2000;2000
        4;10000;20000
        8;;500
        8;;12000

Options:

	-h, --help            
							show this help message and exit

	-s SPEED, --link-speed=SPEED
							specify link speed in kb/s. Default is getting output
							link speed with ethtool.

	-a AP_MBR, --ap-mbr=AP_MBR
							specify ap_mbr in kb/s. Default is 100000.
							
	-m MODE, --mode=MODE
							mode real : delay = radio delay + scheduling delay;
							mode worst-case : delay = max-delay.
							Default is real. See -d for setting up radio delay.

	--mtu=MTU             
							expected MTU. The lower the expected MTU is, the
							better non-GBR bearers' delay requirements will be
							respected. Default is 1600.

	-d PROP_DELAY, --prop-delay=PROP_DELAY
							delay introduced by radio propagation in ms.
							Only considered when mode is real. Default is 50.

	-j PROP_JITTER, --prop-jitter=PROP_JITTER
							jitter introduced by radio propagation in ms.
							Default is 0.

	-i INPUT_IFACE, --input-iface=INPUT_IFACE
							output interface. Default is eth1.

	-o OUTPUT_IFACE, --output-iface=OUTPUT_IFACE
							output interface. Default is eth2.
							
	-n, --no-global-overflow
							if true, size of bfifos will be calculated according
							to their htb's ceil

Auteurs : Tristan Ninet
