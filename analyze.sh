#!/bin/bash

pkill tcpdump
pcap_latency_analyzer-master/pcap_latency --file-diff --full-packet-udp-only eth1.pcap eth2.pcap --latency-histo --latency-histo-unit 1000 --latency-histo-min 0 --latency-histo-max 1000000 > output
# pcap_latency_analyzer-master/pcap_latency --file-diff --full-packet-udp-only eth0.pcap eth1.pcap --latency-histo > output
cat output | grep "\*" | awk '{ print($1,$4) }' > graph
# scp graph tristan@192.168.1.100:
