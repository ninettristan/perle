#coding:utf8

import os, sys, commands
import csv
from optparse import OptionParser

nb_bearers = 0 # total number of bearers
total_rate = 0 # total guaranteed rate in kb/s
delay_distribution = "normal" # {  uniform  |  normal  |  pareto |  paretonormal } (see netem documentation)

# initialize speed, ap_mbr, mode and initial qdisc architecture
def init():

    global conf,speed,ap_mbr,mode,mtu,prop_delay,prop_jitter,input_iface,output_iface,no_global_overflow


    # parses the options and initialize global variables
    usage = """usage: %prog [options] CONF
    where CONF is the csv configuration file.
    Example of configuration file :
        "qci";"gbr";"mbr"
        5;;9000
        1;1000;4000
        3;2000;2000
        4;10000;20000
        8;;500
        8;;12000"""
    parser = OptionParser(usage)

    parser.add_option("-s", "--link-speed",
            dest="speed", default=0,
            help="""specify link speed in kb/s. Default is getting output link speed with ethtool.""" )
    parser.add_option("-a", "--ap-mbr",
            dest="ap_mbr", default=100000,
            help="""specify ap_mbr in kb/s. Default is 100000.""" )
    parser.add_option("-m", "--mode",
            dest="mode", default="real",
            help="""mode real : delay = radio delay + scheduling delay;
            mode worst-case : delay = max-delay.
            Default is real. See -d for setting up radio delay.""" )
    parser.add_option("--mtu", 
            dest="mtu", default=1600,
            help="""expected MTU. The lower the expected MTU is, the better non-GBR bearers' delay requirements will be respected. Default is 1600.""" )
    parser.add_option("-d", "--prop-delay",
            dest="prop_delay", default=50,
            help="""delay introduced by radio propagation in ms.
            Only considered when mode is real. Default is 50.""" )
    parser.add_option("-j", "--prop-jitter",
            dest="prop_jitter", default=0,
            help="""jitter introduced by radio propagation in ms.
            Default is 0.""" )
    parser.add_option("-i", "--input-iface",
            dest="input_iface", default="eth1",
            help="""output interface. Default is eth1.""" )
    parser.add_option("-o", "--output-iface",
            dest="output_iface", default="eth2",
            help="""output interface. Default is eth2.""" )
    parser.add_option("-n", "--no-global-overflow", action="store_true",
            dest="no_global_overflow",
            help="""if true, size of bfifos will be calculated according to their htb's ceil""" )

    (options, args) = parser.parse_args()

    ap_mbr = int(options.ap_mbr)
    mode = options.mode
    mtu = int(options.mtu)
    prop_delay = int(options.prop_delay)

    prop_jitter = int(options.prop_jitter)
    if prop_jitter==0:
        prop_jitter = 0.001

    if len(args)!=1:
        exit("You must provide the configuration file")
    conf = args[0]

    input_iface=options.input_iface
    output_iface=options.output_iface
    no_global_overflow=bool(options.no_global_overflow)

    # if speed was not given, we ask kernel for output interface speed with ethtool
    if options.speed==0:
        cmd = "ethtool "+output_iface+" | grep -i speed | cut -d ' ' -f 2 | cut -d 'M' -f 1"
        try:
            speed = 1000*int(commands.getstatusoutput(cmd)[1])/5  # link speed in kb/s
        except ValueError:
            sys.exit("failed to get output speed, please install ethtool and run perle as root")
    else:
        speed = options.speed

    # qdisc initialization
    print ("qdisc initialization")
    os.system("""
        modprobe ifb numifbs=1
        ip link set dev ifb0 up

        tc qdisc del dev """+input_iface+""" ingress
        tc qdisc add dev """+input_iface+""" handle ffff: ingress
        tc filter add dev """+input_iface+""" parent ffff: protocol ip u32 match u32 0 0 action mirred egress redirect dev ifb0

        tc qdisc del dev ifb0 root
        tc qdisc add dev ifb0 root handle 1: prio bands 15

        tc qdisc del dev """+output_iface+""" root
        tc qdisc add dev """+output_iface+""" root handle 1: htb
        tc class add dev """+output_iface+""" parent 1: classid 1:1 htb rate """+str(speed)+"""kbit
        tc class add dev """+output_iface+""" parent 1:1 classid 1:2 htb rate 1kbit ceil """+str(ap_mbr)+"""kbit
    """)




# adds bearer of QCI qci, GBR gbr and MBR mbr
def addBearer(qci,gbr,mbr) :
    global nb_bearers,total_rate

    # gets qdisc parameters according to QCI
    qci = int(qci)
    if qci==1 :
        # qci 1. VOIP call. gbr
        type = "gbr"
        prio = 2
        max_delay = 100 # in milliseconds
        loss = 1 # in %
        parent = "1:1"
    elif qci==2 :
        # qci 2. video call. gbr
        type = "gbr"
        prio = 4
        max_delay = 150 # in milliseconds
        loss = 0.1 # in %
        parent = "1:1"
    elif qci==3 :
        # qci 3. online gaming. gbr
        type = "gbr"
        prio = 3
        max_delay = 50 # in milliseconds
        loss = 0.1 # in %
        parent = "1:1"
    elif qci==4 :
        # qci 4. video streaming. gbr
        type = "gbr"
        prio = 5
        max_delay = 300 # in milliseconds
        loss = 0.0001 # in %
        parent = "1:1"
    elif qci==5 :
        # qci 5. IMS signaling
        type = "non-gbr"
        prio = 1
        max_delay = 100 # in milliseconds
        loss = 0.0001 # in %
        parent = "1:2"
    elif qci==6 :
        # qci 6. email, chat, ftp
        type = "non-gbr"
        prio = 6
        max_delay = 300 # in milliseconds
        loss = 0.0001 # in %
        parent = "1:2"
    elif qci==7 :
        # qci 7. voice
        type = "non-gbr"
        prio = 7
        max_delay = 100 # in milliseconds
        loss = 0.1 # in %
        parent = "1:2"
    elif qci==8 :
        # qci 8. email, chat, ftp
        type = "non-gbr"
        prio = 8
        max_delay = 300 # in milliseconds
        loss = 0.0001 # in %
        parent = "1:2"
    elif qci==9 :
        # qci 9. email, chat, ftp
        type = "non-gbr"
        prio = 9
        max_delay = 300 # in milliseconds
        loss = 0.0001 # in %
        parent = "1:2"
    else :
        sys.exit("You ask for a QCI that doesn't exist : "+str(qci)+"\n")


    # sets qdisc rate and ceil values according to type, gbr and mbr
    if type=="gbr":
        if gbr=="" and mbr=="":
            rate = 50000 # in kb/s, i.e. kb/s/s
            ceil = 50000
        elif mbr=="":
            rate = int(gbr)
            ceil = int(gbr)
        elif gbr=="":
            rate = int(mbr)
            ceil = int(mbr)
        else:
            rate = int(gbr)
            ceil = int(mbr)
        total_rate += rate
        if total_rate>speed:
            print("Error : bearer not added because link speed is not sufficient")
            total_rate -= rate
            return
        else:
            announce = "adding     GBR bearer of type : "+str(qci)+", gbr : "+str(rate)+"kb/s, mbr : "+str(ceil)+"kb/s"
    else:
        rate=1
        if mbr=="":
            ceil = ap_mbr
        else:
            ceil = int(mbr)
        announce = "adding non-GBR bearer of type : "+str(qci)+", ceil "+str(ceil)

    #### calculates bfifo's length and guaranteed delay
    # Note that in netem : ms=milliseconds, whereas in pcap_analyzer : ns=microseconds

    # If we know that there will be no global overflow, the lower throughput to which we will be shaping is the ceil, and thus for GBR and non-GBR bearers. So :
    if no_global_overflow:
        min_shaping_throughput = ceil
    else:
        min_shaping_throughput = rate

    # limit(bit) = (max_delay(s)-prop_delay(s))*rate(bit/s)
    # We have max_delay and prop_delay in milliseconds, rate in kbit/s, and we want limit in bytes, so :
    limit = (max_delay-prop_delay)*min_shaping_throughput/8
    if limit<mtu:
        limit = mtu
        actual_max_delay = int(limit/min_shaping_throughput*8+prop_delay)
    else:
        actual_max_delay = max_delay
    if type=="gbr":
        announce += ", max_delay : "+str(actual_max_delay)+" ms"
    elif max_delay>prop_delay:
        necessary_rate = limit/(max_delay-prop_delay)*8
        announce += ", max_delay : "+str(max_delay)+"ms only if accorded rate is higher than "+str(necessary_rate)+"kb/s"
    else:
        announce+=", delay cannot be guaranteed"

    print(announce)

    # sets netem delay according to mode
    if mode=="worst-case":
        netem_delay = max_delay
    else:
        netem_delay = prop_delay

    # Setting up the corresponding qdiscs
    os.system("tc class add dev "+output_iface+" parent "+parent+" classid 1:"
            +str(10+nb_bearers)+" htb rate "+str(rate)+"kbit ceil "
            +str(ceil)+"kbit prio "+str(prio))

    os.system("tc qdisc add dev "+output_iface+" parent 1:"
            +str(10+nb_bearers)+" handle "+str(10+nb_bearers)
            +": bfifo limit "+str(limit))

    os.system("tc filter add dev "+output_iface
            +" protocol ip parent 1:0 prio 1 u32 match ip dport "
            +str(5000+nb_bearers)+" 0xffff flowid 1:"+str(10+nb_bearers))

    os.system("tc qdisc add dev ifb0 parent 1:"+str(1+nb_bearers)
            +" handle "+str(20+nb_bearers)+": netem delay "
            +str(netem_delay)+"ms "+str(prop_jitter)+"ms distribution "
            +delay_distribution+" loss "+str(loss)+"%")

    os.system("tc filter add dev ifb0 protocol ip parent 1:0 prio 1 u32 match ip dport "
            +str(5000+nb_bearers)+" 0xffff flowid 1:"+str(1+nb_bearers))

    # keeps track of number of bearers
    nb_bearers += 1


def main():

    init()

    #Adds bearers from conf csv file
    with open(conf,"rb") as file:
        reader = csv.DictReader(file,delimiter = ';')
        for row in reader:
            addBearer(row['qci'],row['gbr'],row['mbr'])

    print ("there are "+str(nb_bearers)+" bearers and a total guaranteed bandwidth of "+str(total_rate)+"kb/s")
    print ("the link speed is : "+str(speed)+"kb/s and the ap_mbr is : "+str(ap_mbr)+"kb/s")


# start of program
main()
