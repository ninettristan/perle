#!/bin/bash

modprobe ifb numifbs=1
ip link set dev ifb0 up

tc qdisc del dev eth1 ingress
tc qdisc add dev eth1 handle ffff: ingress
tc filter add dev eth1 parent ffff: protocol ip u32 match u32 0 0 action mirred egress redirect dev ifb0

tc qdisc del dev ifb0 root
tc qdisc add dev ifb0 root handle 1: prio

tc qdisc del dev eth2 root
tc qdisc add dev eth2 root handle 1: htb
#tc class add dev eth2 parent 1: classid 1:1 htb rate """+str(speed)+"""kbit
#tc class add dev eth2 parent 1:1 classid 1:2 htb rate 1kbit ceil """+str(ap_mbr)+"""kbit

